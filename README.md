## Instructions

Install dependenties with pip (in a virtual environment)

> pip install -r requirements.txt

## Notebooks

Use the webcam or a video stored in your device
> using_web_cam.ipynb


Use pictures
> using_pictures.ipynb


## More resources:

https://github.com/opencv/opencv/tree/master/data/haarcascades